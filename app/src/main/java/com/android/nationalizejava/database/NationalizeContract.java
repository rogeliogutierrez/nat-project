package com.android.nationalizejava.database;

import android.provider.BaseColumns;

public class NationalizeContract{

        public abstract class NationalizeEntry implements BaseColumns {
            public static final String TABLE_NAME ="nationalize";
            public static final String UID = "id";
            public static final String NAME = "name";
    }
}
