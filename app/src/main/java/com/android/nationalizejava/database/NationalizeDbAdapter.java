package com.android.nationalizejava.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.android.nationalizejava.database.NationalizeContract.NationalizeEntry;
import com.android.nationalizejava.model.Nationalize;

import java.util.ArrayList;
import java.util.List;

public class NationalizeDbAdapter{

    NationalizeDbHelper nationalizeHelper;

    public NationalizeDbAdapter(Context context)
    {
        nationalizeHelper = new NationalizeDbHelper(context);
    }

    public long insertData(String name)
    {
        SQLiteDatabase dbb = nationalizeHelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(NationalizeEntry.NAME, name);
        long id = dbb.insert(NationalizeEntry.TABLE_NAME, null , contentValues);
        return id;
    }

    public List<Nationalize> getData()
    {
        List<Nationalize> nationalizeList = new ArrayList<Nationalize>();
        SQLiteDatabase db = nationalizeHelper.getWritableDatabase();
        String[] columns = {NationalizeEntry.UID, NationalizeEntry.NAME};
        Cursor cursor =db.query(NationalizeEntry.TABLE_NAME,columns,null,null,null,null,null);
        StringBuffer buffer= new StringBuffer();
        while (cursor.moveToNext())
        {
            int cid =cursor.getInt(cursor.getColumnIndex(NationalizeEntry.UID));
            String name = cursor.getString(cursor.getColumnIndex(NationalizeEntry.NAME));
           // buffer.append(cid+ "   " + name +" \n");
            nationalizeList.add(new Nationalize(name, null));
        }
        return nationalizeList;
    }

    public  int delete(String uname)
    {
        SQLiteDatabase db = nationalizeHelper.getWritableDatabase();
        String[] whereArgs ={uname};

        int count =db.delete(NationalizeEntry.TABLE_NAME ,NationalizeEntry.NAME+" = ?",whereArgs);
        return  count;
    }

    public int updateName(String oldName , String newName)
    {
        SQLiteDatabase db = nationalizeHelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(NationalizeEntry.NAME,newName);
        String[] whereArgs= {oldName};
        int count =db.update(NationalizeEntry.TABLE_NAME,contentValues, NationalizeEntry.NAME+" = ?",whereArgs );
        return count;
    }

    static class NationalizeDbHelper extends  SQLiteOpenHelper {
        public static final int DATABASE_VERSION = 1;
        public static final String DATABASE_NAME = "NationalizeDb.db";

        public NationalizeDbHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            // Create table...
            db.execSQL("CREATE TABLE " + NationalizeEntry.TABLE_NAME + " ("
                    + NationalizeEntry.UID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + NationalizeEntry.NAME + " TEXT NOT NULL,"
                    + "UNIQUE (" + NationalizeEntry.UID + "))");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }
    }
}
