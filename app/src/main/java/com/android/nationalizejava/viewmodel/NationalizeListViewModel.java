package com.android.nationalizejava.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.android.nationalizejava.model.Nationalize;
import com.android.nationalizejava.network.APIService;
import com.android.nationalizejava.network.RetroInstance;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NationalizeListViewModel extends ViewModel {
    private MutableLiveData<Nationalize> nationalizeList;

    public NationalizeListViewModel(){
        nationalizeList = new MutableLiveData<>();
    }

    public MutableLiveData<Nationalize> getNatListObserver() {
        return nationalizeList;
    }

    public void makeApiCall(String textSearch) {
        APIService apiService = RetroInstance.getRetroClient().create(APIService.class);
        Call<Nationalize> call = apiService.getNatList(textSearch);
        call.enqueue(new Callback<Nationalize>() {
            @Override
            public void onResponse(Call<Nationalize> call, Response<Nationalize> response) {
                nationalizeList.postValue(response.body());
            }

            @Override
            public void onFailure(Call<Nationalize> call, Throwable t) {
                nationalizeList.postValue(null);
            }
        });
    }
}
