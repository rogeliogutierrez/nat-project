package com.android.nationalizejava.network;

import com.android.nationalizejava.model.Nationalize;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface APIService {

    @GET("/")
    Call<Nationalize> getNatList(
            @Query("name") String query
    );
}
