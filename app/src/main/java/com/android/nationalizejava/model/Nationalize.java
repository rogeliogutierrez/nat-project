package com.android.nationalizejava.model;

import java.io.Serializable;
import java.util.List;

public class Nationalize implements Serializable {
    public String name;
    public List<Country> country;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Country> getCountry() {
        return country;
    }

    public void setCountry(List<Country> country) {
        this.country = country;
    }

    public Nationalize(String name, List<Country> country) {
        this.name = name;
        this.country = country;
    }
}
