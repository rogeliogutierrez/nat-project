package com.android.nationalizejava.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.nationalizejava.R;
import com.android.nationalizejava.adapter.CountryListAdapter;
import com.android.nationalizejava.model.Country;
import com.android.nationalizejava.model.Nationalize;
import com.android.nationalizejava.viewmodel.NationalizeListViewModel;
import com.google.android.material.textfield.TextInputEditText;

import java.util.List;

import com.android.nationalizejava.database.NationalizeDbAdapter;

public class MainActivity extends AppCompatActivity implements CountryListAdapter.ItemClickListener {

    private List<Country> natModelList;
    private CountryListAdapter adapter;
    private NationalizeListViewModel viewModel;
    private NationalizeDbAdapter nationalizeDbAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        nationalizeDbAdapter = new NationalizeDbAdapter(this);

        Button viewHistory = findViewById(R.id.viewHistory);
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        TextView noresult = findViewById(R.id.noResultTv);
        TextInputEditText search = findViewById(R.id.text_search);

        search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    viewModel = ViewModelProviders.of(MainActivity.this).get(NationalizeListViewModel.class);
                    viewModel.makeApiCall(textView.getText().toString());
                    viewModel.getNatListObserver().observe(MainActivity.this, new Observer<Nationalize>() {
                        @Override
                        public void onChanged(Nationalize nationalize) {
                            if(nationalize != null) {
                                if(nationalize.getCountry().size() > 0) {
                                    nationalizeDbAdapter.insertData(nationalize.getName());
                                    natModelList = nationalize.getCountry();
                                    adapter.setNatList(nationalize.getCountry());
                                    noresult.setVisibility(View.GONE);
                                }
                            } else {
                                noresult.setVisibility(View.VISIBLE);
                            }
                        }
                    });
                }
                return false;
            }
        });

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        adapter =  new CountryListAdapter(this, natModelList, this);
        recyclerView.setAdapter(adapter);


        viewHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), HistoryActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onCountryClick(Country country) {
        Toast.makeText(this, "Clicked list name "+country.getCountry_id()+", probability is: : " +country.getProbability(), Toast.LENGTH_SHORT).show();
    }
}