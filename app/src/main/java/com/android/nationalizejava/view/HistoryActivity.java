package com.android.nationalizejava.view;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.nationalizejava.R;
import com.android.nationalizejava.adapter.CountryListAdapter;
import com.android.nationalizejava.adapter.NationalizeListAdapter;
import com.android.nationalizejava.database.NationalizeDbAdapter;
import com.android.nationalizejava.model.Country;
import com.android.nationalizejava.model.Nationalize;

public class HistoryActivity extends AppCompatActivity implements NationalizeListAdapter.ItemClickListener {
    private NationalizeListAdapter adapter;
    private NationalizeDbAdapter nationalizeDbAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.history_main);

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        Button viewHome = findViewById(R.id.viewHome);
        nationalizeDbAdapter = new NationalizeDbAdapter(this);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        adapter =  new NationalizeListAdapter(this, nationalizeDbAdapter.getData(), this);
        recyclerView.setAdapter(adapter);

        viewHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), MainActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onNationalizeClick(Nationalize nationalize){
        Toast.makeText(this, "Clicked list name is : " +nationalize.getName(), Toast.LENGTH_SHORT).show();
    }
}
