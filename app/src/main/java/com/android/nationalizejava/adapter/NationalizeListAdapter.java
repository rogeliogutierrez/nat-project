package com.android.nationalizejava.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.nationalizejava.R;
import com.android.nationalizejava.model.Country;
import com.android.nationalizejava.model.Nationalize;

import java.util.List;

public class NationalizeListAdapter extends RecyclerView.Adapter<NationalizeListAdapter.MyViewHolder> {
    private Context context;
    private List<Nationalize> nationalizeList;
    private ItemClickListener clickListener;

    public NationalizeListAdapter(Context context, List<Nationalize> nationalizeList, ItemClickListener clickListener) {
        this.context = context;
        this.nationalizeList = nationalizeList;
        this.clickListener = clickListener;
    }

    public void setNatList(List<Nationalize> nationalizeList) {
        this.nationalizeList = nationalizeList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public NationalizeListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.recycler_row, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.tvTitle.setText(this.nationalizeList.get(position).getName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListener.onNationalizeClick(nationalizeList.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        if (this.nationalizeList != null) {
            return this.nationalizeList.size();
        }
        return 0;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.titleView);
        }
    }


    public interface ItemClickListener {
        public void onNationalizeClick(Nationalize nationalize);
    }
}