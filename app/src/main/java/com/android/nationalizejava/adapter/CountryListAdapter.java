package com.android.nationalizejava.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.nationalizejava.R;
import com.android.nationalizejava.model.Country;

import java.util.List;

public class CountryListAdapter extends RecyclerView.Adapter<CountryListAdapter.MyViewHolder> {
    private Context context;
    private List<Country> countryList;
    private ItemClickListener clickListener;

    public CountryListAdapter(Context context, List<Country> countryList, ItemClickListener clickListener) {
        this.context = context;
        this.countryList = countryList;
        this.clickListener = clickListener;
    }

    public void setNatList(List<Country> countryList) {
        this.countryList = countryList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public CountryListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.recycler_row, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.tvTitle.setText(this.countryList.get(position).getCountry_id());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListener.onCountryClick(countryList.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        if (this.countryList != null) {
            return this.countryList.size();
        }
        return 0;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.titleView);
        }
    }


    public interface ItemClickListener {
        public void onCountryClick(Country nationalize);
    }
}